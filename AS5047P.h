/**
 ********************************************************************************
 * @file    AS5047P.h
 * @author  Yaraslau Martsinkevich
 * @date    May 21, 2022
 * @brief   
 ********************************************************************************
 */

#ifndef AS5047P_AS5047P_H_
#define AS5047P_AS5047P_H_

#ifdef __cplusplus
extern "C" {
#endif

/************************************
 * INCLUDES
 ************************************/
#include <stdint.h>
/************************************
 * MACROS AND DEFINES
 ************************************/
#define AS5047P_NOP				(uint16_t) 0x0000
#define AS5047P_ERRFL			(uint16_t) 0x0001
#define AS5047P_PROG			(uint16_t) 0x0003
#define AS5047P_DIAAGC			(uint16_t) 0x3FFC
#define AS5047P_MAG				(uint16_t) 0x3FFD
#define AS5047P_ANGLEUNC		(uint16_t) 0x3FFE
#define AS5047P_ANGLECOM		(uint16_t) 0x3FFF

#define AS5047P_ZPOSM			(uint16_t) 0x0016
#define AS5047P_ZPOSL			(uint16_t) 0x0017
#define AS5047P_SETTINGS1		(uint16_t) 0x0018
#define AS5047P_SETTINGS2		(uint16_t) 0x0019

#define AS5047P_WRITE			(uint16_t) 0x0000
#define AS5047P_READ			(uint16_t) 0x0001

#define AS5047P_RESOLUTION		(float) 360.0 / ( 1 << 14 )
/************************************
 * TYPEDEFS
 ************************************/
typedef int8_t (*as5047p_Transfer)(void *, uint16_t *, uint16_t *);
typedef void (*as5047p_Delay)(uint32_t);

typedef struct
{
	as5047p_Transfer pxTransfer;
	as5047p_Delay	pxDelay;
	void * pvParameter;
} as5047_Handle_t;

typedef union {
	uint16_t raw;
	struct {
		uint16_t addr 			: 14;
		uint16_t rw				: 1;
		uint16_t parc			: 1;
	} fields;
} as5047p_CommandFrame_t;

typedef struct
{
	uint16_t data 			: 14;
	uint16_t ef				: 1;
	uint16_t pard			: 1;
} as5047p_ReadDataFrame_t;

typedef struct
{
	uint16_t data 			: 14;
	uint16_t o				: 1;
	uint16_t pard			: 1;
} as5047p_WriteDataFrame_t;

typedef struct
{
	uint16_t frerr			: 1;
	uint16_t invcomm		: 1;
	uint16_t parerr			: 1;
	uint16_t reserved		: 13;
} as5047p_ErrflReg_t;

typedef struct
{
	uint16_t progen			: 1;
	uint16_t reserved_0		: 1;
	uint16_t otpref			: 1;
	uint16_t progotp		: 1;
	uint16_t reserved_1		: 2;
	uint16_t progver		: 1;
	uint16_t reserved_2		: 9;
} as5047p_ProgReg_t;

typedef struct
{
	uint16_t agc			: 8;
	uint16_t lf				: 1;
	uint16_t cof			: 1;
	uint16_t magh			: 1;
	uint16_t magl			: 1;
	uint16_t reserved		: 4;
} as5047p_DiaagcReg_t;

typedef struct
{
	uint16_t data			: 14;
	uint16_t reserved		: 2;
} as5047p_MagReg_t;

typedef struct
{
	uint16_t data			: 14;
	uint16_t reserved		: 2;
} as5047p_AngleuncReg_t;

typedef struct
{
	uint16_t data			: 14;
	uint16_t reserved		: 2;
} as5047p_AnglecomReg_t;
/************************************
 * EXPORTED VARIABLES
 ************************************/

/************************************
 * GLOBAL FUNCTION PROTOTYPES
 ************************************/
int8_t scAs5047_ReadAngleCompensated(as5047_Handle_t * pxAs5047, float * pfAngle);
int8_t scAs5047_ReadDiaagc( as5047_Handle_t * pxAs5047 );
int8_t scAs5047_ReadErrorRegister( as5047_Handle_t * pxAs5047,  as5047p_ErrflReg_t * xErrReg );


#ifdef __cplusplus
}
#endif

#endif 
