# C library for AS5047p sensor
### In order to use this library, you need to take the following steps:

* Write your own function to transfer 16 bits by SPI.
* Create variable of the "as5047_Handle_t" type.
* Assign the variable with a pointer to the transfer function.
* Now you can use the function to read an angle, by passing pointers to the sensor handle and float variable.

You can find the prototype of the SPI transfer function in the header file or in the example, below.

In addition, you could use second field of the "as5047_Handle_t" as an optional pointer to the user defined varible which will be passed in the transfer function.

# Library compatible with next sensors:
* AS5047p
* AS5047D
* AS5147
* AS5147P
* AS5247

# STM32 HAL simplified example:
```C
/*
*   Structure which holds SPI bus interface and utilized as optional parameter
*/
typedef struct
{
	SPI_HandleTypeDef 	* xSpi;
	GPIO_TypeDef 		* xCsPort;
	uint16_t 			uxCsPin;
} SpiBusHandle_t;
/* SPI transfer function prototype */
int8_t scAs5047_Transfer( void *pvBus, uint16_t * pxTxData, uint16_t *pxRxData);

int main(void)
{
    HAL_StatusTypeDef xStatus;
    /* Create and assing SPI bus interface variable */
    SpiBusHandle_t xAs5047_Bus = { &hspi3, SPI3_CS_GPIO_Port, SPI3_CS_Pin };
    /* Create and assing AS5047p handle */
    as5047_Handle_t xAs5047 = { scAs5047_Transfer, &xAs5047_Bus };
    /* Variable that holds angle */
    float xAngle = 0.0;

    while(1)
    {
        /* Wait 1 sec */
        HAL_Delay( 1000 );
        /* Perforn sensor reading */
        xStatus = scAs5047_ReadAngleCompensated( &xAs5047, &xAngle );
        /* Send this data to the PC terminal */
        printf( "Compensated angle: %f\n", xAngle );
    }
}

/* SPI transfer function definition */
int8_t scAs5047_Transfer( void *pvBus, uint16_t * pxTxData, uint16_t *pxRxData)
{
	int8_t scStatus;
	SpiBusHandle_t *xBus = pvBus;

	/* CS goes down (start of the transmission) */
	HAL_GPIO_WritePin( xBus->xCsPort, xBus->uxCsPin, GPIO_PIN_RESET);
	/* SPI transfer of 16 bits */
	scStatus = HAL_SPI_TransmitReceive( xBus->xSpi, (uint8_t *)pxTxData, (uint8_t *)pxRxData, 1, 0xff );
	/* CS goes up (end of the transmission) */
	HAL_GPIO_WritePin( xBus->xCsPort, xBus->uxCsPin, GPIO_PIN_SET);

	return scStatus;
}
```