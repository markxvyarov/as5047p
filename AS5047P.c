/**
 ********************************************************************************
 * @file    AS5047P.c
 * @author  Yaraslau Martsinkevich
 * @date    May 21, 2022
 * @brief   
 ********************************************************************************
 */

/************************************
 * INCLUDES
 ************************************/
#include "AS5047P.h"
/************************************
 * EXTERN VARIABLES
 ************************************/

/************************************
 * PRIVATE MACROS AND DEFINES
 ************************************/

/************************************
 * PRIVATE TYPEDEFS
 ************************************/

/************************************
 * STATIC VARIABLES
 ************************************/

/************************************
 * GLOBAL VARIABLES
 ************************************/

/************************************
 * STATIC FUNCTION PROTOTYPES
 ************************************/
static uint16_t uxGetEvenParity(uint16_t uxInput);
/************************************
 * STATIC FUNCTIONS
 ************************************/
uint16_t uxGetEvenParity(uint16_t uxInput)
{
	uint16_t uxParity = 0;

	while(uxInput != 0)
	{
		uxParity ^= uxInput;
		uxInput >>= 1;
	}

	return (uxParity & 0x1);
}
/************************************
 * GLOBAL FUNCTIONS
 ************************************/
int8_t scAs5047_ReadAngleCompensated(as5047_Handle_t * pxAs5047, float * pfAngle)
{
	int8_t scStatus;
	as5047p_CommandFrame_t xCommand = { 0 };
	as5047p_ReadDataFrame_t xRead;

	xCommand.fields.addr = AS5047P_ANGLECOM;
	xCommand.fields.rw = AS5047P_READ;
	xCommand.fields.parc = uxGetEvenParity( xCommand.raw );

	scStatus = pxAs5047->pxTransfer( pxAs5047->pvParameter, &xCommand.raw, (uint16_t*)&xRead );

	if( scStatus == 0)
	{
		xCommand.raw = 0;

		xCommand.fields.addr = AS5047P_NOP;
		xCommand.fields.rw = AS5047P_READ;
		xCommand.fields.parc = uxGetEvenParity( xCommand.raw );

		scStatus = pxAs5047->pxTransfer( pxAs5047->pvParameter, &xCommand.raw, (uint16_t*)&xRead );

		*pfAngle = (float)xRead.data * AS5047P_RESOLUTION;
	}

	return scStatus;
}

int8_t scAs5047_ReadDiaagc( as5047_Handle_t * pxAs5047 )
{
	int8_t scStatus;
	as5047p_CommandFrame_t xCommand = { 0 };
	as5047p_ReadDataFrame_t xRead;

	xCommand.fields.addr = AS5047P_DIAAGC;
	xCommand.fields.rw = AS5047P_READ;
	xCommand.fields.parc = uxGetEvenParity( xCommand.raw );

	scStatus = pxAs5047->pxTransfer( pxAs5047->pvParameter, &xCommand.raw, (uint16_t*)&xRead );

//	pxAs5047->pxDelay( 1 );

	if( scStatus == 0)
	{
		xCommand.raw = 0;

		xCommand.fields.addr = AS5047P_NOP;
		xCommand.fields.rw = AS5047P_READ;
		xCommand.fields.parc = uxGetEvenParity( xCommand.raw );

		scStatus = pxAs5047->pxTransfer( pxAs5047->pvParameter, &xCommand.raw, (uint16_t*)&xRead );

	}

	return scStatus;
}

int8_t scAs5047_ReadErrorRegister( as5047_Handle_t * pxAs5047,  as5047p_ErrflReg_t * xErrReg )
{
	int8_t scStatus;
	as5047p_CommandFrame_t xCommand = { 0 };
	as5047p_ReadDataFrame_t xRead;

	xCommand.fields.addr = AS5047P_ERRFL;
	xCommand.fields.rw = AS5047P_READ;
	xCommand.fields.parc = uxGetEvenParity( xCommand.raw );

	scStatus = pxAs5047->pxTransfer( pxAs5047->pvParameter, &xCommand.raw, (uint16_t*)&xRead );

	if( scStatus == 0)
	{
		xCommand.raw = 0;

		xCommand.fields.addr = AS5047P_NOP;
		xCommand.fields.rw = AS5047P_READ;
		xCommand.fields.parc = uxGetEvenParity( xCommand.raw );

		scStatus = pxAs5047->pxTransfer( pxAs5047->pvParameter, &xCommand.raw, (uint16_t*)xErrReg );
	}

	return scStatus;
}






